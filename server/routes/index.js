var express = require('express')
var router = express.Router()

var album_controller = require('../controllers/AlbumController')
var artist_controller = require('../controllers/ArtistController')

router.get('/search', artist_controller.search_artists)

router.get('/artist/:id', artist_controller.get_albums_for_artist)

router.get('/album/:id', album_controller.get_album)

module.exports = router
