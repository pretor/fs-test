import axios from 'axios'

const API_KEY = '91abf181073c4a46ee0be675f40c782b'
const API_URL = 'http://ws.audioscrobbler.com/2.0'
const API_FORMAT = 'json'

exports.get_album = async function(mbid) {
 return axios.get(API_URL +'/?method=album.getInfo&mbid='+ mbid +'&api_key='+ API_KEY +'&format=' + API_FORMAT)

};

exports.search_artists = async function(artist) {
  return axios.get(API_URL +'/?method=artist.search&artist='+ artist +'&api_key='+ API_KEY +'&format=' + API_FORMAT)
};

exports.get_albums_for_artist = async function(mbid) {
  return axios.get(API_URL +'/?method=artist.getTopAlbums&mbid='+ mbid +'&api_key='+ API_KEY +'&format=' + API_FORMAT)
};
