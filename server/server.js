import express from 'express'
import request from 'request'
const cors = require('cors');
/* eslint-disable no-console */

const port = process.env.PORT || 3001
const app = express()
const env = process.env.NODE_ENV || 'development'

if(env === 'development') app.use(cors({ origin: true }))

var indexRouter = require('./routes/index')

app.use('/api', indexRouter)

app.listen(port, err => {
  if (err) {
    console.error(err)
    process.exit(1)
  } else {
    console.log('Server listening: http://localhost:%s', port)
  }
})
