var apiService = require('../services/apiService')

exports.search_artists = async function(req, res) {
  try {
    var artist = req.query.artist;
    const response = await apiService.search_artists(artist)
    res.status(200).json(response.data)
  } catch(e) {
    console.log(e.stack)
    res.status(500).send({error: e.message})
  }
}

exports.get_albums_for_artist = async function(req, res) {
  try {
    let mbid = req.query.mbid
    const response = await apiService.get_albums_for_artist(req.params.id)
    res.status(200).json(response.data)
  } catch(e) {
    console.log(e.stack)
    res.status(500).send({error: e.message})
  }
}