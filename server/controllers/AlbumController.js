var apiService = require('../services/apiService');

exports.get_album = async function(req, res) {
  try {
    const album = await apiService.get_album(req.params.id)
    console.log(album)
    res.status(200).json(album.data);
  } catch(e) {
    console.log(e.stack)
    res.status(500).send({error: e.message})
  }
}
