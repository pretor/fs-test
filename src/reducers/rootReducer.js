import { FETCH_ALBUM_ERROR, FETCH_ALBUM_PENDING, FETCH_ALBUM_SUCCESS, FETCH_ALBUMS_ERROR, FETCH_ALBUMS_PENDING,
  FETCH_ALBUMS_SUCCESS, SEARCH_ARTISTS_ERROR, SEARCH_ARTISTS_PENDING, SEARCH_ARTISTS_SUCCESS,PUSH_BREADCRUMB, POP_BREADCRUMB, RESET_BREADCRUMB } from '../actions/action'

const initState = {
  artists: [],
  albums: {
    album: []
  },
  album: {
    artist: 're',
    name: 'Test',
    image: [{},{},{},{'#text': ''}],
    tracks: {
      track: [],
    }
  },
}

const rootReducer = (state = initState, action) => {

  switch(action.type) {
    case FETCH_ALBUM_PENDING:
      return {
        ...state,
        pending: true
      }
    case FETCH_ALBUM_SUCCESS:
      return {
        ...state,
        pending: false,
        album: action.payload
      }
    case FETCH_ALBUM_ERROR:
      return {
        ...state,
        pending: false,
        error: action.error
      }
    case FETCH_ALBUMS_PENDING:
      return {
        ...state,
        pending: true
      }
    case FETCH_ALBUMS_SUCCESS:
      return {
        ...state,
        pending: false,
        albums: action.payload
      }
    case FETCH_ALBUMS_ERROR:
      return {
        ...state,
        pending: false,
        error: action.error
      }
    case SEARCH_ARTISTS_PENDING:
      return {
        ...state,
        pending: true
      }
    case SEARCH_ARTISTS_SUCCESS:
      return {
        ...state,
        pending: false,
        artists: action.payload
      }
    case SEARCH_ARTISTS_ERROR:
      return {
        ...state,
        pending: false,
        error: action.error
      }
    case PUSH_BREADCRUMB:
      return {
        ...state,
        breadcrumbs: [...state.breadcrumbs, action.payload]
      }
    case POP_BREADCRUMB:
      if(state.breadcrumbs.length > 1){
        state.breadcrumbs.pop()
      }

      return {
        ...state,
        breadcrumbs: state.breadcrumbs
      }
    case RESET_BREADCRUMB:
      return {
        ...state,
        breadcrumbs: [{text: 'Search', link: '/search'}]
      }
    default:
      return state
  }
}

export const getAlbum = state => state.album
export const getAlbumPending = state => state.pending
export const getAlbumError = state => state.error

export const getAlbums = state => state.albums
export const getAlbumsPending = state => state.pending
export const getAlbumsError = state => state.error

export const getArtists = state => state.artists
export const getArtistsPending = state => state.pending
export const getArtistsError = state => state.error

export const getBreadcrumbs = state => state.breadcrumbs

export default rootReducer
