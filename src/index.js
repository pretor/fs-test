import React from 'react'
import ReactDOM from 'react-dom'
import { Router } from 'react-router'
import { Switch, Route } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import App from './App'
import NotFoundPage from './containers/NotFound'
import SearchPage from './containers/SearchPage'
import AlbumPage from './containers/AlbumPage'
import TracksPage from './containers/TracksPage'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer'

const history = createBrowserHistory()

const middlewares = [thunk];

const initState = {
  breadcrumbs: [{
    text: 'Search', link: '/search', position: 1
  }],
  artists: [],
  albums: {
    album: [],
    "@attr": {
      artist: ''
    }
  },
  album: {
    artist: 're',
    name: 'Test',
    image: [{},{},{},{'#text': ''}],
    tracks: {
      track: [],
    }
  },
}

const store = createStore(rootReducer, initState, applyMiddleware(...middlewares));


ReactDOM.render(
  <Provider store={store}>
  <Router history={history}>
    <App>
      <Switch>
        <Route exact path="/" component={SearchPage} />
        <Route exact path="/search" component={SearchPage} />
        <Route path="/album/:id" component={AlbumPage} />
        <Route exact path="/track/:id" component={TracksPage} />
        <Route component={NotFoundPage} />
      </Switch>
    </App>
  </Router>
  </Provider>,
  document.getElementById('root')
)
