import React from 'react'
import Loader from 'react-loader-spinner'

class Spinner extends React.Component {

  render() {
    if (this.props.pending) {
    return <div className="container text-center"><Loader
        type="Puff"
        color="#00BFFF"
        height="100"
        width="100"
    /></div>
    } else {
      return <div></div>
    }
  }
}

export default Spinner
