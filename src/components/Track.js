import React from 'react'

class Track extends React.Component {

  formatTime = (time) =>{
    let minutes = Math.floor(time / 60)
    let seconds = time - minutes * 60
    if(seconds < 10){
      seconds = '0' + seconds
    }
    return minutes + ':' + seconds
  }

  render() {
    return (
      <li className="list-group-item">
        {this.props.id + 1}. {this.props.track.name} <span className="badge">{this.formatTime(this.props.track.duration)}</span>
      </li>
    )
  }
}

export default Track
