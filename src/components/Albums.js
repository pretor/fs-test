import React from 'react'
import Album from './Album'

class Albums extends React.Component {

  render() {
    return (
      <div className="container">
        <div className="row" style={{ justifyContent: 'space-between' }}>
          {this.props.albums.map((album) => (
            <Album album={album}/>
          ))}
        </div>
      </div>
    )
  }
}

export default Albums
