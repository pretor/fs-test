import React from 'react'
import Tracks from './Tracks'

class AlbumImage extends React.Component {

  render() {
    let album = this.props.album
    return (
      <div className="col">
        <img
          src={album.image[3]['#text']}
          className="thumbnail img-responsive"
          alt={album.name}
        />
      </div>
    )
  }
}

export default AlbumImage
