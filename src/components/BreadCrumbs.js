import React from 'react'
import { Link  } from 'react-router-dom'

class BreadCrumbs extends React.Component {
  render() {
    return (
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              {this.props.breadcrumbs.map((breadcrumb, id) => (
                <li className="breadcrumb-item">
                  <Link to={breadcrumb.link}>{breadcrumb.text}</Link>
                </li>
              ))}

            </ol>
          </nav>
    )
  }
}

export default BreadCrumbs
