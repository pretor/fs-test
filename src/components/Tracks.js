import React from 'react'
import Track from './Track'

class Tracks extends React.Component {

  render() {
    return (
      <div className="col">
        <ul className="list-group">
          {this.props.tracks.map((track, id) => (
            <Track track={track} id={id}/>
          ))}
        </ul>
      </div>
    )
  }
}

export default Tracks
