import React from 'react'
import { Link } from 'react-router-dom'

class Artists extends React.Component {

  render() {
    return (
      <div className="container artists">
        {this.props.artists.map((artist) => (
          <div className="media-container">
            <Link to={`/album/${artist.mbid}`}>
              <div className="media">
                <img
                  className="align-self-start mr-3"
                  src={artist.image[0]['#text']}
                  alt=""
                />
                <div className="media-body">
                  <h5 className="mt-0">{artist.name}</h5>
                  Listeners: {artist.listeners}
                </div>
              </div>
            </Link>
          </div>
        ))}
      </div>
    )
  }
}

export default Artists
