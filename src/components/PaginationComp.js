import React from 'react'
import Pagination from "react-js-pagination";

class PaginationComp extends React.Component {

  render() {
    const artists = this.props.artists
    if (artists.length > 0) {
      return <div className="container text-center">
        <nav aria-label="Page navigation example">
          <Pagination
            activePage={this.props.activePage}
            itemsCountPerPage={this.props.perPage}
            totalItemsCount={artists.length}
            pageRangeDisplayed={5}
            onChange={this.props.handlePageChange}
          /></nav></div>
    } else {
      return <div></div>
    }
  }
}

export default PaginationComp
