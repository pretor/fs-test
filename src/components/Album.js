import React from 'react'
import { Link } from 'react-router-dom'

class Album extends React.Component {

  render() {
    let album = this.props.album
    return (
      <Link to={`/track/${album.mbid}`}>
        <div className="card" style={{ width: '14rem' }}>
          <img
            className="card-img-top"
            src={album.image[2]['#text']}
            alt="Card image cap"
          />
          <div className="card-body">
            <p className="card-text">{album.name}</p>
          </div>
        </div>
      </Link>
    )
  }
}

export default Album
