import React from 'react'

class Error extends React.Component {

  render() {
    if (this.props.show) {
      return<div className="container text-center">
        <div className="alert alert-danger" role="alert">
          An error occurred!
        </div>
      </div>
    } else {
      return <div></div>
    }
  }
}

export default Error
