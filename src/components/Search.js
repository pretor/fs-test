import React from 'react'

class Search extends React.Component {

  render() {
   return (
     <div className="panel panel-default">
       <div className="panel-heading">Rechercher un artiste Spotify</div>
       <div className="panel-body">
         <form className="form-inline" onSubmit={this.props.onSubmit}>
           <div className="form-group">
             <input
               type="search"
               className="form-control"
               placeholder="Mot(s)-clé(s)"
               value={this.props.value}
               onChange={this.props.onChange}
             />
           </div>
           <button type="submit" className="btn btn-primary">
             Search
           </button>
         </form>
       </div>
     </div>
   )
  }
}

export default Search
