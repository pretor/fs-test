import React from 'react'
import { getAlbums, getAlbumsError, getAlbumsPending, getBreadcrumbs } from '../reducers/rootReducer'
import { bindActionCreators } from 'redux'
import  fetchAlbumsAction from '../actions/fetchAlbums'
import { popBreadcrumb, pushBreadcrumb } from '../actions/action'
import { connect } from 'react-redux'
import BreadCrumbs from '../components/BreadCrumbs'
import Spinner from '../components/Spinner'
import Error from '../components/Error'
import Albums from '../components/Albums'

class AlbumPage extends React.Component {

  componentWillMount() {
    let mbid = this.props.match.params.id
    const {fetchAlbums} = this.props
    const {pushBreadcrumb} = this.props
    fetchAlbums(mbid)
    pushBreadcrumb({text: 'Artist', link: '/album/'+mbid, position: 2})
  }

  componentWillUnmount() {
    const {popBreadcrumb} = this.props
  }

  render() {
    const {albums} = this.props

    return (
      <div>
        <div className="container">
          <BreadCrumbs page={'artist'} breadcrumbs={this.props.breadcrumbs}/>
          <div className="page-header">
            <h1>Albums</h1>
            <h2>{albums['@attr'].artist}</h2>
          </div>
          <Spinner pending={this.props.pending}/>
          <Error show={this.props.error}/>
          <Albums albums={albums.album}/>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
return {
  error: getAlbumsError(state),
  albums: getAlbums(state),
  pending: getAlbumsPending(state),
  breadcrumbs: getBreadcrumbs(state),
}}

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchAlbums: fetchAlbumsAction,
  pushBreadcrumb: pushBreadcrumb,
  popBreadcrumb: popBreadcrumb,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(AlbumPage)
