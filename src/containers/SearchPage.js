import React from 'react'
import { getArtists, getArtistsError, getArtistsPending } from '../reducers/rootReducer'
import { bindActionCreators } from 'redux'
import searchArtistsAction from '../actions/searchArtists'
import { connect } from 'react-redux'
import Spinner from '../components/Spinner'
import Error from '../components/Error'
import Artists from '../components/Artists'
import PaginationComp from '../components/PaginationComp'
import Search from '../components/Search'
import { resetBreadcrumb } from '../actions/action'

class SearchPage extends React.Component {

  constructor(){
    super()
    this.state = {value: '', activePage: 1, perPage: 5, offset: 0};
  }

  handlePageChange = (pageNumber) => {
    this.setState({activePage: pageNumber});
    this.setState({offset: (Number(pageNumber) - 1) * this.state.perPage});
  }

  getArtists = () => {
    return this.props.artists.filter((art,idx) => idx >= this.state.offset && idx <= this.state.offset + this.state.perPage)
  }

  onSearchValueChange = (e) => {
    this.setState({ value: e.target.value })
  }

  componentWillMount() {
    const {resetBreadcrumb} = this.props
    resetBreadcrumb()
  }

  render() {
    const {artists} = this.props;
    return (
      <div>
        <div>
          <div className="container">
            <div className="page-header">
              <h1>Artistes</h1>
            </div>
            <Search value={this.state.value} onChange={this.onSearchValueChange} onSubmit={this.search} />
          </div>
          <Spinner pending={this.props.pending}/>
          <Artists artists={this.getArtists()}/>
          <Error show={this.props.error}/>
          <br/>
          <PaginationComp artists={artists} handlePageChange={this.handlePageChange} activePage={this.state.activePage}
                          perPage={this.state.perPage} />
        </div>
      </div>
    )
  }

  search = (e) => {
    e.preventDefault()
    const {searchArtists} = this.props
    searchArtists(this.state.value)
  }
}

const mapStateToProps = state => ({
  error: getArtistsError(state),
  artists: getArtists(state),
  pending: getArtistsPending(state),
  value: '',
})

const mapDispatchToProps = dispatch => bindActionCreators({
      searchArtists: searchArtistsAction,
      resetBreadcrumb: resetBreadcrumb,

}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage)
