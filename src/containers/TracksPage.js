import React from 'react'
import { connect } from 'react-redux'
import { getAlbumError, getAlbum, getAlbumPending, getBreadcrumbs } from '../reducers/rootReducer'
import { bindActionCreators } from 'redux'
import fetchAlbumAction from '../actions/fetchAlbum'
import  {pushBreadcrumb, popBreadcrumb } from '../actions/action'
import BreadCrumbs from '../components/BreadCrumbs'
import Spinner from '../components/Spinner'
import Error from '../components/Error'
import AlbumImage from '../components/AlbumImage'
import Tracks from '../components/Tracks'

class TracksPage extends React.Component {

  componentWillMount() {
    let mbid = this.props.match.params.id
    const {fetchAlbum} = this.props
    fetchAlbum(mbid)
    const {pushBreadcrumb} = this.props
    pushBreadcrumb({text: 'Album', link: '/track/'+mbid, position: 3})
  }

  componentWillUnmount() {
    const {popBreadcrumb} = this.props
    popBreadcrumb(3)
  }

  render() {
    const {album} = this.props

    return (
      <div>
        <div className="container">
          <BreadCrumbs page={'album'} breadcrumbs={this.props.breadcrumbs} />

          <div className="page-header">
            <h1>Pistes</h1>
            <h2>
              {album.artist} - {album.name}
            </h2>
          </div>
          <Spinner pending={this.props.pending}/>
          <Error show={this.props.error}/>
          <div className="container">
            <div className="row" style={{ justifyContent: 'space-between' }}>
              <AlbumImage album={album}/>
              <Tracks tracks={album.tracks.track}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  error: getAlbumError(state),
  album: getAlbum(state),
  pending: getAlbumPending(state),
  breadcrumbs: getBreadcrumbs(state),
})

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchAlbum: fetchAlbumAction,
  pushBreadcrumb: pushBreadcrumb,
  popBreadcrumb: popBreadcrumb,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TracksPage)
