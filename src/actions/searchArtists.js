import {searchArtistsPending, searchArtistsSuccess, searchArtistsError} from './action';

function searchArtists(artist) {
  return dispatch => {
    dispatch(searchArtistsPending());
    fetch('http://localhost:3001/api/search?artist=' + artist)
      .then(res => res.json())
      .then(res => {
        if(res.error) {
          throw(res.error);
        }
        dispatch(searchArtistsSuccess(res.results.artistmatches.artist))
        return res;
      })
      .catch(error => {
        console.log(error)
        dispatch(searchArtistsError(error))
      })
  }
}

export default searchArtists;