import {fetchAlbumPending, fetchAlbumSuccess, fetchAlbumError} from './action';

function fetchAlbum(id) {
  return dispatch => {
    dispatch(fetchAlbumPending());
    fetch('http://localhost:3001/api/album/' + id)
      .then(res => res.json())
      .then(res => {
        if(res.error) {
          throw(res.error);
        }
        dispatch(fetchAlbumSuccess(res.album))
        return res;
      })
      .catch(error => {
        console.log(error)
        dispatch(fetchAlbumError(error))
      })
  }
}

export default fetchAlbum;