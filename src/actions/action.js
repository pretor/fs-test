export const FETCH_ALBUM_PENDING = 'FETCH_ALBUM_PENDING'
export const FETCH_ALBUM_SUCCESS = 'FETCH_ALBUM_SUCCESS'
export const FETCH_ALBUM_ERROR = 'FETCH_ALBUM_ERROR'

export const FETCH_ALBUMS_PENDING = 'FETCH_ALBUMS_PENDING'
export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS'
export const FETCH_ALBUMS_ERROR = 'FETCH_ALBUMS_ERROR'

export const SEARCH_ARTISTS_PENDING = 'SEARCH_ARTISTS_PENDING'
export const SEARCH_ARTISTS_SUCCESS = 'SEARCH_ARTISTS_SUCCESS'
export const SEARCH_ARTISTS_ERROR = 'SEARCH_ARTISTS_ERROR'

export const PUSH_BREADCRUMB = 'PUSH_BREADCRUMB'
export const POP_BREADCRUMB = 'POP_BREADCRUMB'
export const RESET_BREADCRUMB = 'RESET_BREADCRUMB'

export function fetchAlbumPending() {
  return {
    type: FETCH_ALBUM_PENDING,
  }
}

export function fetchAlbumSuccess(album) {
  return {
    type: FETCH_ALBUM_SUCCESS,
    payload: album,
  }
}

export function fetchAlbumError(error) {
  return {
    type: FETCH_ALBUM_ERROR,
    error: error,
  }
}

export function fetchAlbumsPending() {
  return {
    type: FETCH_ALBUMS_PENDING,
  }
}

export function fetchAlbumsSuccess(album) {
  return {
    type: FETCH_ALBUMS_SUCCESS,
    payload: album,
  }
}

export function fetchAlbumsError(error) {
  return {
    type: FETCH_ALBUMS_ERROR,
    error: error,
  }
}

export function searchArtistsPending() {
  return {
    type: SEARCH_ARTISTS_PENDING,
  }
}

export function searchArtistsSuccess(album) {
  return {
    type: SEARCH_ARTISTS_SUCCESS,
    payload: album,
  }
}

export function searchArtistsError(error) {
  return {
    type: SEARCH_ARTISTS_ERROR,
    error: error,
  }
}

export function pushBreadcrumb(payload){
  return {
  type: PUSH_BREADCRUMB,
  payload : payload,
  }
}

export function popBreadcrumb(){
  return {
    type: POP_BREADCRUMB,
  }
}

export function resetBreadcrumb(){
  return {
    type: RESET_BREADCRUMB,
  }
}

