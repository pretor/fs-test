import {fetchAlbumsPending, fetchAlbumsSuccess, fetchAlbumsError} from './action';

function fetchAlbums(id) {
  return dispatch => {
    dispatch(fetchAlbumsPending());
    fetch('http://localhost:3001/api/artist/' + id)
      .then(res => res.json())
      .then(res => {
        if(res.error) {
          throw(res.error);
        }
        dispatch(fetchAlbumsSuccess(res.topalbums))
        return res;
      })
      .catch(error => {
        console.log(error)
        dispatch(fetchAlbumsError(error))
      })
  }
}

export default fetchAlbums;